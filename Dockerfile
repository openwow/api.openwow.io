FROM mcr.microsoft.com/dotnet/core/sdk:2.2-bionic AS build

WORKDIR /app
RUN git clone https://gitlab.com/openwow/common.git/

WORKDIR /app/api.openwow.io
COPY . .
RUN dotnet test

WORKDIR /app/api.openwow.io/src/OpenWoW.ApiWeb
RUN dotnet publish -c Release -o out


FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS runtime
WORKDIR /app/api.openwow.io
COPY --from=build /app/api.openwow.io/src/OpenWoW.ApiWeb/out ./

ENV ASPNETCORE_URLS "http://0:5000"
EXPOSE 5000
ENTRYPOINT ["dotnet", "OpenWoW.ApiWeb.dll"]
