﻿using Microsoft.AspNetCore.Mvc;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.ApiWeb.Controllers
{
    [ApiController]
    [Route("v{version:apiVersion}/[controller]")]
    public class BuildsController : ControllerBase
    {
        private readonly IWebRepository _repository;

        public BuildsController(IWebRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public async Task<List<BuildData>> GetAll()
        {
            return await _repository.GetAllBuildData();
        }

        [HttpGet("{branch}")]
        public async Task<List<BuildData>> GetByBranch([FromRoute]WowBranch branch)
        {
            return await _repository.GetAllBuildDataByBranch(branch);
        }

        [HttpGet("{id:int}")]
        [ProducesResponseType(200, Type = typeof(BuildData))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<BuildData>> GetById(int id)
        {
            var buildData = await _repository.GetBuildDataById(id);
            if (buildData == null)
            {
                return NotFound();
            }
            return buildData;
        }
    }
}
