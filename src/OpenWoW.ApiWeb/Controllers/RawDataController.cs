﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OpenWoW.Common;
using OpenWoW.Database.Web.CompositeEntities;
using OpenWoW.Database.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.ApiWeb.Controllers
{
    [ApiController]
    [Route("v{version:apiVersion}/[controller]")]
    public class RawDataController : ControllerBase
    {
        private readonly IWebRepository _repository;
        private readonly ILogger<RawDataController> _logger;

        public RawDataController(IWebRepository repository, ILogger<RawDataController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("{locale}/{branch}/{tableName:regex(^[[a-zA-Z0-9_-]]+$)}")]
        [ProducesResponseType(200, Type = typeof(RawDataJson))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<RawDataJson>> Get([FromRoute]WowLocale locale, [FromRoute]WowBranch branch, [FromRoute]string tableName)
        {
            var buildData = await _repository.GetLatestBuildByBranch(branch);
            if (buildData == null)
            {
                return NotFound();
            }

            var tableHashLookup = await _repository.GetTableHashLookupByName(tableName);
            if (tableHashLookup == null)
            {
                return NotFound();
            }

            var dbconfig = await _repository.GetDbconfigLookupByBuildIdAndTableHash(buildData.Id, tableHashLookup._TableHash);
            if (dbconfig == null)
            {
                return NotFound();
            }

            var result = new RawDataJson
            {
                Version = buildData.Version,
                Build = buildData.Build,
                Fields = dbconfig.FieldNames,
            };

            var records = await _repository.GetRawDataByLocaleAndBuildIdAndTableHash(locale, buildData.Id, tableHashLookup._TableHash);
            if (records.Count > 0)
            {
                result.Records = records;
            }

            return result;
        }

        [HttpGet("{locale}/{branch}/{tableName:regex(^[[a-zA-Z0-9_-]]+$)}/{rowId:int}")]
        [ProducesResponseType(200, Type = typeof(RawDataJson))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<RawDataJson>> GetById([FromRoute]WowLocale locale, [FromRoute]WowBranch branch, [FromRoute]string tableName, [FromRoute]int rowId)
        {
            var buildData = await _repository.GetLatestBuildByBranch(branch);
            if (buildData == null)
            {
                return NotFound();
            }

            var metadata = await _repository.GetRawMetadataByLocaleAndBuildIdAndTableNameAndRowId(locale, buildData.Id, tableName, rowId);
            if (metadata == null)
            {
                return NotFound();
            }

            var data = await _repository.GetRawDataByRowHash(metadata.RowHash);
            if (data == null)
            {
                return NotFound();
            }

            var dbconfig = await _repository.GetDbconfigLookupByBuildIdAndTableHash(buildData.Id, metadata._TableHash);
            if (dbconfig == null)
            {
                return NotFound();
            }

            return new RawDataJson
            {
                Version = buildData.Version,
                Build = buildData.Build,
                Fields = dbconfig.FieldNames,
                Records = new List<RawDataWithId>
                {
                    new RawDataWithId
                    {
                        Id = rowId,
                        Data = JsonConvert.DeserializeObject<object[]>(data.JsonData),
                    },
                },
            };
        }
    }
}
