﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.ApiWeb.Controllers
{
    [ApiController]
    [Route("v{version:apiVersion}/[controller]")]
    public class DataController : ControllerBase
    {
        private readonly IWebRepository _repository;
        private readonly ILogger<DataController> _logger;

        public DataController(IWebRepository repository, ILogger<DataController> logger)
        {
            _repository = repository;
            _logger = logger;
        }

        private async Task<SubSite> GetSubSite(WowBranch branch)
        {
            var site = await _repository.GetSubSiteByBranch(branch);
            return site?.BuildId.HasValue == true ? site : null;
        }

        [HttpGet("{locale}/{branch}/classes")]
        [ProducesResponseType(200, Type = typeof(List<WowClass>))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<WowClass>>> GetClasses([FromRoute]WowLocale locale, [FromRoute]WowBranch branch)
        {
            var site = await GetSubSite(branch);
            if (site == null)
            {
                return NotFound();
            }

            return Ok(await _repository.GetAllWowClassByBuildId(site.BuildId.Value));
        }

        [HttpGet("{locale}/{branch}/currency_categories")]
        [ProducesResponseType(200, Type = typeof(List<WowCurrencyCategory>))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<WowClass>>> GetCurrencyCategories([FromRoute]WowLocale locale, [FromRoute]WowBranch branch)
        {
            var site = await GetSubSite(branch);
            if (site == null)
            {
                return NotFound();
            }

            return Ok(await _repository.GetAllWowCurrencyCategoryByBuildId(site.BuildId.Value));
        }

        [HttpGet("{locale}/{branch}/races")]
        [ProducesResponseType(200, Type = typeof(List<WowRace>))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<WowRace>>> GetRaces([FromRoute]WowLocale locale, [FromRoute]WowBranch branch)
        {
            var site = await GetSubSite(branch);
            if (site == null)
            {
                return NotFound();
            }

            return Ok(await _repository.GetAllWowRaceByBuildId(site.BuildId.Value));
        }

        [HttpGet("{locale}/{branch}/skill_lines")]
        [ProducesResponseType(200, Type = typeof(List<WowSkillLine>))]
        [ProducesResponseType(404)]
        public async Task<ActionResult<List<WowSkillLine>>> GetSkillLines([FromRoute]WowLocale locale, [FromRoute]WowBranch branch)
        {
            var site = await GetSubSite(branch);
            if (site == null)
            {
                return NotFound();
            }

            return Ok(await _repository.GetAllWowSkillLineByBuildId(site.BuildId.Value));
        }
    }
}
