﻿using OpenWoW.Database.Web.CompositeEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.ApiWeb
{
    public class RawDataJson
    {
        public string Version;
        public int Build;
        public string[] Fields;
        public List<RawDataWithId> Records;
    }
}
